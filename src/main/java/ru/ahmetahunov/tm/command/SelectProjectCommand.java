package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.ConsoleHelper;
import ru.ahmetahunov.tm.model.Project;
import ru.ahmetahunov.tm.model.ProjectCollection;
import ru.ahmetahunov.tm.model.Task;
import ru.ahmetahunov.tm.model.TaskCollection;

import java.io.IOException;
import java.util.Set;

public class SelectProjectCommand implements Command {
    @Override
    public void execute() throws IOException {
        System.out.println("[SELECT PROJECT]");
        System.out.print("Please enter project name: ");
        String name = ConsoleHelper.readMessage().trim();
        Project project = ProjectCollection.getProject(name);
        if (project == null) {
            System.out.println("Selected project not exists.");
            return;
        }
        System.out.println(project.getName());
        Set<Integer> taskIds = project.getTasks();
        if (taskIds.size() != 0)
            System.out.println("Task:");
        int i = 1;
        for (int id : taskIds) {
            Task task = TaskCollection.getTask(id);
            if (task != null) {
                System.out.println(String.format("  %d. %s", i++, task.getName()));
            }
        }
    }

    @Override
    public String toString() {
        return "select-project: Show selected project.";
    }
}
