package ru.ahmetahunov.tm.command;

class ExitCommand implements Command {
    public void execute() {
        System.out.println("Have a nice day!");
    }

    public String toString() {
        return "exit: exit from task manager";
    }
}