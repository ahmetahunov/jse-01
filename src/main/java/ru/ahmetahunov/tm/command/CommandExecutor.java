package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.Operation;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class CommandExecutor {
    static final Map<Operation, Command> commands = new TreeMap<>();

    static {
        commands.put(Operation.HELP, new HelpCommand());
        commands.put(Operation.PROJECT_CLEAR, new ProjectClearCommand());
        commands.put(Operation.PROJECT_CREATE, new ProjectCreateCommand());
        commands.put(Operation.PROJECT_LIST, new ProjectListCommand());
        commands.put(Operation.PROJECT_REMOVE, new ProjectRemoveCommand());
        commands.put(Operation.TASK_CLEAR, new TaskClearCommand());
        commands.put(Operation.TASK_CREATE, new TaskCreateCommand());
        commands.put(Operation.TASK_REMOVE, new TaskRemoveCommand());
        commands.put(Operation.TASK_LIST, new TaskListCommand());
        commands.put(Operation.EXIT, new ExitCommand());
        commands.put(Operation.SELECT_PROJECT, new SelectProjectCommand());
    }

    public static void execute(Operation operation) throws IOException {
        if (operation == Operation.UNKNOWN) {
            System.out.println("Unknown operation." +
                    " Please enter correct command or 'help' to list available operations.");
            System.out.println();
            return;
        }
        Command command = commands.get(operation);
        command.execute();
    }
}