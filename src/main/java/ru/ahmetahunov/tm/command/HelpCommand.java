package ru.ahmetahunov.tm.command;

class HelpCommand implements Command {
    public void execute() {
        for (Command command : CommandExecutor.commands.values()) {
            System.out.println(command.toString());
        }
        System.out.println();
    }

    public String toString() {
        return "help: Show all commands.";
    }
}