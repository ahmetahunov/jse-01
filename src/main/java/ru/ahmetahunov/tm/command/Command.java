package ru.ahmetahunov.tm.command;

import java.io.IOException;

interface Command {
    void execute() throws IOException;
}