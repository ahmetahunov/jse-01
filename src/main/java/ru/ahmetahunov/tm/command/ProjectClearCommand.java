package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.model.ProjectCollection;

class ProjectClearCommand implements Command {
    @Override
    public void execute() {
        ProjectCollection.clear();
        System.out.println("[ALL PROJECTS REMOVED]");
        System.out.println();
    }

    public String toString() {
        return "project-clear: Remove all projects.";
    }
}