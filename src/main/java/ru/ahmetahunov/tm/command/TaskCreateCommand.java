package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.ConsoleHelper;
import ru.ahmetahunov.tm.model.Project;
import ru.ahmetahunov.tm.model.ProjectCollection;
import ru.ahmetahunov.tm.model.Task;
import ru.ahmetahunov.tm.model.TaskCollection;

import java.io.IOException;

class TaskCreateCommand implements Command {
    @Override
    public void execute() throws IOException {
        System.out.println("[TASK CREATE]");
        System.out.println("If you want create new task for available project " +
                "enter project name\nor press enter to skip");
        Project project = getProject();
        System.out.print("Please enter task name: ");
        String name = ConsoleHelper.readMessage().trim();
        Task task = TaskCollection.getTask(name, (project == null) ? 0 : project.getId());
        if (task != null) {
            System.out.println(name + " already exists.");
            if (!isReplacedTask(task, project))
                return;
        }
        task = new Task(name);
        TaskCollection.add(task);
        if (project != null) {
            task.setProjectId(project.getId());
            project.getTasks().add(task.getId());
        }
        System.out.println("[OK]");
        System.out.println();
    }

    private Project getProject() throws IOException {
        String name = ConsoleHelper.readMessage().trim();
        if (!("".equals(name))) {
            Project project = ProjectCollection.getProject(name);
            if (project != null)
                return project;
            String answer = null;
            System.out.println(name + " is not available.");
            do {
                System.out.println("Do you want use another project?<y/n>");
                answer = ConsoleHelper.readMessage();
                if ("y".equals(answer))
                    return getProject();
            } while (!("n".equals(answer)));
        }
        return null;
    }

    private boolean isReplacedTask(Task task, Project project) throws IOException {
        String answer;
        do {
            System.out.println("Do you want replace it?<y/n>");
            answer = ConsoleHelper.readMessage();
            if ("n".equals(answer)) {
                System.out.println("[CANCELLED]");
                System.out.println();
                return false;
            }
        }while (!("y".equals(answer)));
        if (project != null)
            project.getTasks().remove(task.getId());
        TaskCollection.remove(task);
        return true;
    }

    @Override
    public String toString() {
        return "task-create: Create new task.";
    }
}