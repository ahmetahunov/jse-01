package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.model.TaskCollection;

class TaskClearCommand implements Command {
    @Override
    public void execute() {
        TaskCollection.clear();
        System.out.println("[ALL TASKS REMOVED]");
        System.out.println();
    }

    @Override
    public String toString() {
        return "task-clear: Remove all tasks.";
    }
}