package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.ConsoleHelper;
import ru.ahmetahunov.tm.model.Project;
import ru.ahmetahunov.tm.model.ProjectCollection;
import ru.ahmetahunov.tm.model.Task;
import ru.ahmetahunov.tm.model.TaskCollection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

class ProjectRemoveCommand implements Command {
    @Override
    public void execute() throws IOException {
        System.out.println("[PROJECT REMOVE]");
        System.out.print("Please enter project name: ");
        String name = ConsoleHelper.readMessage().trim();
        for (Project project : new ArrayList<>(ProjectCollection.getProjects())) {
            if (project.getName().equals(name)) {
                ProjectCollection.remove(project);
                Map<Integer, Task> tasks = TaskCollection.getTasksMap();
                for (int taskId : project.getTasks()) {
                    tasks.remove(taskId);
                }
                System.out.println("[OK]");
                System.out.println();
                return;
            }
        }
        System.out.println("Selected project not exists.");
        System.out.println();
    }

    @Override
    public String toString() {
        return "project-remove: Remove selected project.";
    }
}