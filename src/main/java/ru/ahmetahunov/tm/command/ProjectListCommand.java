package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.model.Project;
import ru.ahmetahunov.tm.model.ProjectCollection;
import ru.ahmetahunov.tm.model.Task;
import ru.ahmetahunov.tm.model.TaskCollection;

import java.util.*;

class ProjectListCommand implements Command {
    @Override
    public void execute() {
        int i = 1;
        System.out.println("[PROJECT LIST]");
        for (Project project : ProjectCollection.getProjects()) {
            System.out.println(String.format("%d. %s", i++, project.getName()));
            int j = 1;
            Collection<Task> tasks = getTasks(project.getTasks());
            if (tasks.size() != 0)
                System.out.println("Task:");
            for (Task task : tasks) {
                System.out.println(String.format("  %d) %s", j++, task.getName()));
            }
        }
        System.out.println();
    }

    private Collection<Task> getTasks(Set<Integer> ids) {
        List<Task> tasks = new ArrayList<>();
        Map<Integer, Task> allTasks = TaskCollection.getTasksMap();
        for (int id : ids) {
            tasks.add(allTasks.get(id));
        }
        return tasks;
    }

    @Override
    public String toString() {
        return "project-list: Show all projects with tasks.";
    }
}