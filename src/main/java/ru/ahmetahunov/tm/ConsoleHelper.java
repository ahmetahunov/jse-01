package ru.ahmetahunov.tm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleHelper {
    private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static String readMessage() throws IOException {
        return reader.readLine();
    }

    public static void close() throws IOException {
        reader.close();
    }
}
