package ru.ahmetahunov.tm.model;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class TaskCollection {
    private static final Map<Integer, Task> tasks = new LinkedHashMap<>();

    public static Collection<Task> getTasks() {
        return tasks.values();
    }

    public static Task getTask(String name, int projectId) {
        for (Task task : getTasks()) {
            if (task.getName().equals(name) && task.getProjectId() == projectId)
                return task;
        }
        return null;
    }

    public static Task getTask(int projectId) {
        for (Task task : getTasks()) {
            if (task.getProjectId() == projectId)
                return task;
        }
        return null;
    }

    public static Map<Integer, Task> getTasksMap() { return tasks; }

    public static void add(Task task) {
        tasks.put(task.getId(), task);
    }

    public static void remove(Task task) {
        tasks.remove(task.getId());
    }

    public static void clear() {
        tasks.clear();
    }
}
