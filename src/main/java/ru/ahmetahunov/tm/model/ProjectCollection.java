package ru.ahmetahunov.tm.model;

import java.util.ArrayList;
import java.util.List;

public class ProjectCollection {
    private static final List<Project> projects = new ArrayList<>();

    public static List<Project> getProjects() {
        return projects;
    }

    public static Project getProject(String name) {
        for (Project project : projects) {
            if (project.getName().equals(name))
                return project;
        }
        return null;
    }

    public static void add(Project project) {
        projects.add(project);
    }

    public static void remove(Project project) {
        projects.remove(project);
    }

    public static void clear() {
        projects.clear();
    }
}
